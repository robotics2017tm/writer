classdef collisionCube < handle
    %collisionCube Creates point cloud of cube 
    
    properties
        cubePoints
    end
    
    methods
        function self = collisionCube()
            [Y,Z] = meshgrid(-0.05:0.02:0.05,-0.05:0.02:0.05);      %Creates a plane point cloud for the light curtain cube
            sizeMat = size(Y);
            X = repmat(0.05,sizeMat(1),sizeMat(2));                 %point clound of a Plane is created
            self.cubePoints = [X(:),Y(:),Z(:)]; 
            self.cubePoints = [ self.cubePoints ...
                         ; self.cubePoints * rotz(pi/2)...
                         ; self.cubePoints * rotz(pi) ...
                         ; self.cubePoints * rotz(3*pi/2) ...
                         ; self.cubePoints * roty(pi/2) ...
                         ; self.cubePoints * roty(-pi/2)];  % The same plane is rotated around to create a cube point cloud of edges
            %cube_h = plot3(self.cubePoints(:,1),self.cubePoints(:,2),self.cubePoints(:,3),'b.');

        end
        function collision = checkCube(self,coords)
            self.cubePoints = self.cubePoints + repmat([coords(1),coords(2),coords(3)],size(self.cubePoints,1),1); %Cube Position X Y Z
            %cube_h = plot3(self.cubePoints(:,1),self.cubePoints(:,2),self.cubePoints(:,3),'b.');
            algebraicDist = self.GetAlgebraicDist(self.cubePoints);     % Run the algebraic distance function below
            pointsInside = find(algebraicDist <= 0.02);                 % if the distance is less than 0.02 which is 2 centimeters
            collision = size(pointsInside,1);                           % count the amount of points inside. If this is greater than 0 then it will trigger the light curtain
        end
    end
    methods (Static)
        function algebraicDist = GetAlgebraicDist(points)       %Alegebraic distance from the points to the light curtain
            lightCurtain=-0.39;                                 % position of light curtain
            algebraicDist = abs(points(:,1)-lightCurtain);      %Distance to the light curtain
        end
    end
end

