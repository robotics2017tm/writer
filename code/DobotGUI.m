function varargout = DobotGUI(varargin)
% DOBOTGUI MATLAB code for DobotGUI.fig
%      DOBOTGUI, by itself, creates a new DOBOTGUI or raises the existing
%      singleton*.
%
%      H = DOBOTGUI returns the handle to a new DOBOTGUI or the handle to
%      the existing singleton*.
%
%      DOBOTGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DOBOTGUI.M with the given input arguments.
%
%      DOBOTGUI('Property','Value',...) creates a new DOBOTGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before DobotGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to DobotGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help DobotGUI

% Last Modified by GUIDE v2.5 22-Sep-2017 20:40:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @DobotGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @DobotGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before DobotGUI is made visible.
function DobotGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to DobotGUI (see VARARGIN)

% Choose default command line output for DobotGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes DobotGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = DobotGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function xSlide_Callback(hObject, eventdata, handles)
% hObject    handle to xSlide (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

sliderValuex = get(handles.xSlide,'Value');
sliderValuey = get(handles.ySlide,'Value');
sliderValuez = get(handles.zSlide,'Value');
set(handles.xVal,'String',sprintf('%.1f',sliderValuex));
newPos=[sliderValuex;sliderValuey;sliderValuez]






% --- Executes during object creation, after setting all properties.
function xSlide_CreateFcn(hObject, eventdata, handles)
% hObject    handle to xSlide (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function ySlide_Callback(hObject, eventdata, handles)
% hObject    handle to ySlide (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
sliderValuex = get(handles.xSlide,'Value');
sliderValuey = get(handles.ySlide,'Value');
sliderValuez = get(handles.zSlide,'Value');
set(handles.yVal,'String',sprintf('%.1f',sliderValuey));
newPos=[sliderValuex;sliderValuey;sliderValuez]



% --- Executes during object creation, after setting all properties.
function ySlide_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ySlide (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function zSlide_Callback(hObject, eventdata, handles)
% hObject    handle to zSlide (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
sliderValuex = get(handles.xSlide,'Value');
sliderValuey = get(handles.ySlide,'Value');
sliderValuez = get(handles.zSlide,'Value');
set(handles.zVal,'String',sprintf('%.1f',sliderValuez));
newPos=[sliderValuex;sliderValuey;sliderValuez]


% --- Executes during object creation, after setting all properties.
function zSlide_CreateFcn(hObject, eventdata, handles)
% hObject    handle to zSlide (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function q0_Callback(hObject, eventdata, handles)
% hObject    handle to q0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
sliderValueq0 = get(handles.q0,'Value');
sliderValueq1 = get(handles.q1,'Value');
sliderValueq2 = get(handles.q2,'Value');
sliderValueq3 = get(handles.q3,'Value');

current =[sliderValueq0;sliderValueq1;sliderValueq2;sliderValueq3]
 set(handles.q0T,'String',sprintf('%.1f',sliderValueq0));
  

% --- Executes during object creation, after setting all properties.
function q0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on slider movement.
function q1_Callback(hObject, eventdata, handles)
% hObject    handle to q1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
 sliderValueq0 = get(handles.q0,'Value');
sliderValueq1 = get(handles.q1,'Value');
sliderValueq2 = get(handles.q2,'Value');
sliderValueq3 = get(handles.q3,'Value');

current =[sliderValueq0;sliderValueq1;sliderValueq2;sliderValueq3]
 set(handles.q1T,'String',sprintf('%.1f',sliderValueq1));
 


% --- Executes during object creation, after setting all properties.
function q1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function q2_Callback(hObject, eventdata, handles)
% hObject    handle to q2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
sliderValueq0 = get(handles.q0,'Value');
sliderValueq1 = get(handles.q1,'Value');
sliderValueq2 = get(handles.q2,'Value');
sliderValueq3 = get(handles.q3,'Value');

current =[sliderValueq0;sliderValueq1;sliderValueq2;sliderValueq3]
set(handles.q2T,'String',sprintf('%.1f',sliderValueq2));



% --- Executes during object creation, after setting all properties.
function q2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end




% --- Executes on button press in radiobutton1.
function radiobutton1_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton1


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton2


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
sliderValueZ = get(handles.zSlide,'Value');
sliderValueY = get(handles.ySlide,'Value');
sliderValueX = get(handles.xSlide,'Value');
current=[sliderValueX;sliderValueY;sliderValueZ]

% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
