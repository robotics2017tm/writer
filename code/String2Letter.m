classdef String2Letter
    %String2Letter Takes letter from string
    %   Converts String to Capitals first
    
    properties
    end
    
    methods (Static)
        function out = conv(userString, i)      %Converts the string into individual letters
            userCaps = upper(userString);       % Changes to uppercase
            out = userCaps(i);
        end
    end
    
end

