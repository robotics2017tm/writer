classdef obstacle < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        position
        sliderPos = 0;
        objVerts
        objVertCount
        objMesh_h
    end
    
    methods 
        function self = obstacle(name, pos, rot)
            self.position = [rot pos';0 0 0 1];
            [f,v,data] = plyread(name, 'tri');                           %Collects file data
            self.objVertCount = size(v,1);                               %Finds number of vertices
            midPoint = sum(v)/self.objVertCount;                         %Finds Centre of Model
            self.objVerts = v - repmat(midPoint, self.objVertCount,1);
            vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue]/255; %Converts colors of model
            self.objMesh_h = trisurf(f,v(:,1),v(:,2),v(:,3), 'FaceVertexCData', vertexColours,'EdgeColor', 'interp', 'EdgeLighting','flat');
            shifted = [self.position*[self.objVerts,ones(self.objVertCount,1)]']'; %Calculates new position based on movement
            self.objMesh_h.Vertices = shifted(:,1:3);[rot [pos(1);pos(2);pos(3)];zeros(1,3) 1];
        end
        function moveObstacle(self, dPos)
            moveTR = makehgtform('translate', dPos);
            self.position = self.position*moveTR;
            shifted = [self.position * [self.objVerts,ones(self.objVertCount,1)]']';
            self.objMesh_h.Vertices = shifted(:,1:3);
            drawnow();
        end
    end
    
end

