function varargout = A2UI(varargin)
% A2UI MATLAB code for A2UI.fig
%      A2UI, by itself, creates a new A2UI or raises the existing
%      singleton*.
%
%      H = A2UI returns the handle to a new A2UI or the handle to
%      the existing singleton*.
%
%      A2UI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in A2UI.M with the given input arguments.
%
%      A2UI('Property','Value',...) creates a new A2UI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before A2UI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to A2UI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help A2UI

% Last Modified by GUIDE v2.5 09-Oct-2017 21:41:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @A2UI_OpeningFcn, ...
                   'gui_OutputFcn',  @A2UI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before A2UI is made visible.
function A2UI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to A2UI (see VARARGIN)

% Choose default command line output for A2UI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
% UIWAIT makes A2UI wait for user response (see UIRESUME)
% uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = A2UI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
