classdef textPlotter < handle
    %textPlotter Plots space for letters
    %   Detailed explanation goes here
    
    properties
        userString
        stringLength                                        %Properties of object of the class text plotter
        stringWidth
        plots
        charWidth = 0.015;
        charHeight = 0.03;
        charGap = 0.01;
        raised = 0.1;
    end
    
    methods
        function cObj = textPlotter(textInput)
            cObj.userString = textInput;                            %the objects user input string is the text input into this function
            cObj.stringLength = size(textInput,2);                  %String length is the count from text input 
            cObj.plots = [zeros(cObj.stringLength,3)];              % Creating a matrix for the plot based on the length of string
            cObj.stringWidth = (cObj.stringLength*cObj.charWidth) + ((cObj.stringLength-1)*cObj.charGap);  % Width of the string is based on character width and gap 
        end
        function path = charLines(obj, character, X, Y)             %Function charLines with input of the object, character, and X Y pos
            h = obj.charHeight;                                     %h is the Character height in the object 
            w = obj.charWidth;                                      %w is the Character width in the object
            lift = [0, 0, 0.02];                                    % Creates the kifted centre position
            topL = [X, Y, obj.raised];                              % Creates the position for Top left of the specific letter
            topM = [X+(w/2), Y, obj.raised];                        % Creates the position for Top middle of the specific letter using w
            topR = [X+w, Y, obj.raised];                             % Creates the position for Top right of the specific letter using 2*w
            midL = [X, Y-(h/2), obj.raised];                         % Creates the position for middle left of the specific letter using h
            midM = [X+(w/2), Y-(h/2), obj.raised];                   % Creates the position for middle middle of the specific letter using w and 
            midR = [X+w, Y-(h/2), obj.raised];                       % Creates the position for middle right of the specific letter using 2*w and h
            botL = [X, Y-h, obj.raised];                             % Creates the position for bottom left of the specific letter using 2*h
            botM = [X+(w/2), Y-h, obj.raised];                      % Creates the position for bottom middle of the specific letter using w and 2*h     
            botR = [X+w, Y-h, obj.raised];                          % Creates the position for bottom right of the specific letter using 2*w and 2*h
            switch character                                        %switch case for all the letters based on input character
                case 'A'
                    path = [botL+lift; botL; midL; topM; midR; midL; midR; botR; botR+lift];
                case 'B'
                    path = [midR+lift; midR; botR; botL; topL; topM; midR; midL; midL+lift];
                case 'C'
                    path = [topR+lift; topR; topL; botL; botR; botR+lift];      %Path for letter C is Top right lifted, top right, top left, bottom left, botttom right and bottom right lifted
                case 'D'
                    path = [botL+lift; botL; topL; topM; midR; botM; botL; botL+lift];
                case 'E'
                    path = [topR+lift; topR; topL; midL; midM; midL; botL; botR; botR+lift];
                case 'F'
                    path = [topR+lift; topR; topL; botL; midL; midR; midR+lift];
                case 'G'
                    path = [topR+lift; topR; topL; botL; botR; midR; midM; midM+lift];
                case 'H'
                    path = [topL+lift; topL; botL; midL; midR; botR; topR; topR+lift];
                case 'I'
                    path = [topL+lift; topL; topR; topM; botM; botL; botR; botR+lift];
                case 'J'
                    path = [topM+lift; topM; botM; botL; midL; midL+lift];
                case 'K'
                    path = [topL+lift; topL; botL; midL; topR; midL; botR; botR+lift];
                case 'L'
                    path = [topL+lift; topL; botL; botR; botR+lift];
                case 'M'
                    path = [botL+lift; botL; topL; midM; topR; botR; botR+lift];
                case 'N'
                    path = [botL+lift; botL; topL; botR; topR; topR+lift];
                case 'O'
                    path = [topM+lift; topM; midL; botM; midR; topM; topM+lift];
                case 'P'
                    path = [midL+lift; midL; midR; topR; topL; botL; botL+lift];
                case 'Q'
                    path = [midR+lift; midR; topM; midL; botM; midR; midM; botR; botR+lift];
                case 'R'
                    path = [botL+lift; botL; topL; topR; midR; midL; midM; botR; botR+lift];
                case 'S'
                    path = [topR+lift; topR; topL; midL; midR; botR; botL; botL+lift];
                case 'T'
                    path = [topL+lift; topL; topR; topM; botM; botM+lift];
                case 'U'
                    path = [topL+lift; topL; botL; botR; topR; topR+lift];
                case 'V'
                    path = [topL+lift; topL; midL; botM; midR; topR; topR+lift];
                case 'W'
                    path = [topL+lift; topL; botL; midM; botR; topR; topR+lift];
                case 'X'
                    path = [topL+lift; topL; botR; midM; botL; topR; topR+lift];
                case 'Y'
                    path = [topL+lift; topL; midM; topR; midM; botM; botM+lift];
                case 'Z'
                    path = [topL+lift; topL; topR; botL; botR; botR+lift];
                case '0'
                    path = [topL+lift; topL; botL; botR; topR; topL; topL+lift];
                case '1'
                    path = [midL+lift; midL; topM; botM; botM+lift];
                case '2'
                    path = [midL+lift; midL; topL; topR; midR; botL; botR; botR+lift];
                case '3'
                    path = [topL+lift; topL; topR; midM; midR; botR; botL; botL+lift];
                case '4'
                    path = [botM+lift; botM; topM; midL; midR; midR+lift];
                case '5'
                    path = [topR+lift; topR; topL; midL; midR; botM; botL; botL+lift];
                case '6'
                    path = [topR+lift; topR; topL; botL; botR; midR; midL; midL+lift];
                case '7'
                    path = [topL+lift; topL; topR; botL; botL+lift];
                case '8'
                    path = [topR+lift; topR; topL; botR; botL; topR; topR+lift];
                case '9'
                    path = [midR+lift; midR; midL; topL; topR; botR; botL; botL+lift];
                case ' '
                    path = [];
            end
        end
    end
    methods (Static)
        function graphRect(h, w, x, y) %Not used atm but was used to create a rectangle around each letter for positioning
            plot3([x x+w x+w x x],[y y y-h y-h y],[0 0 0 0 0])
        end
        function graphPlots(obj)                    
            xPlot = -obj.stringWidth/2;         %Xplot is half of the string width
            for i = 1:obj.stringLength          % iterating through the string length
                obj.plots(i,:) = [xPlot, 0.255, obj.raised];%0.225 Splitting the writing are between the letters and positiong them
                xPlot = xPlot + (obj.charWidth + obj.charGap); %adds the character width and gap to space out letters
            end
        end 
        
    end
end
