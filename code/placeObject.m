function placeObject(name, x, y, z, rot)          
    [f,v,data] = plyread(name, 'tri');                      %Collects file data
    objVertCount = size(v,1);                               %Finds number of vertices
    midPoint = sum(v)/objVertCount;                         %Finds Centre of Model
    objVerts = v - repmat(midPoint, objVertCount,1);
    vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue]/255; %Converts colors of model
    objMesh_h = trisurf(f,v(:,1),v(:,2),v(:,3), 'FaceVertexCData', vertexColours,'EdgeColor', 'interp', 'EdgeLighting','flat');
    shifted = [[rot [x;y;z];zeros(1,3) 1]*[objVerts,ones(objVertCount,1)]']'; %Calculates new position based on movement
    objMesh_h.Vertices = shifted(:,1:3);                    %Moves Model
end

