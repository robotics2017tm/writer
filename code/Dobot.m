classdef Dobot < handle
    properties
        %> Robot model
        model;
        
        %>
        workspace = [-2 2 -2 2 -0.3 2];   
        
        
    end
    
    methods%% Class for UR5 robot simulation
function self = Dobot()

%> Define the boundaries of the workspace

        
% robot = 
self.GetDobotRobot();
% robot = 
self.PlotAndColourRobot();%robot,workspace);
end
     
%% GetDobotRobot
% Given a name (optional), create and return a Dobot Model
function GetDobotRobot(self)
%     if nargin < 1
        % Create a unique name (ms timestamp after 1ms pause)
        pause(0.001);
        name = ['Dobot',datestr(now,'yyyymmddTHHMMSSFFF')];
%     end
    
%     L0 = Link('sigma',1,'d',0.128,'a',0,'q',0);%,'alpha',-pi/2);
    L1 = Link('d',0.103,'a',0,'alpha',-pi/2,'offset',0,'qlim',[deg2rad(-180),deg2rad(180)]);
    L2 = Link('d',0.00,'a',0.135,'alpha',0,'offset',0,'qlim',[deg2rad(-180),deg2rad(180)]);
    L3 = Link('d',0.00,'a',0.16012,'alpha',0,'offset',0,'qlim',[deg2rad(-180),deg2rad(180)]);
    L4 = Link('d',0.00,'a',0.0,'alpha',pi/2,'offset',deg2rad(-39.6),'qlim',[deg2rad(-180),deg2rad(180)]);
    L5 = Link('d',0.00,'a',0.0,'alpha',0,'offset',deg2rad (-90),'qlim',[deg2rad(-180),deg2rad(180)]);


    self.model = SerialLink([L1 L2 L3 L4 L5  ],'name',name);
    self.model.teach([0 60 100 0 0])
end        
        
%% PlotAndColourRobot
% Given a robot index, add the glyphs (vertices and faces) and
% colour them in if data is available 
function PlotAndColourRobot(self)%robot,workspace)
    for linkIndex = 0:self.model.n
        [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['DobotLink',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
        self.model.faces{linkIndex+1} = faceData;
        self.model.points{linkIndex+1} = vertexData;
    end

    % Display robot
    self.model.plot3d(zeros(1,self.model.n),'noarrow','workspace',self.workspace);
    if isempty(findobj(get(gca,'Children'),'Type','Light'))
        camlight
    end  
    self.model.delay = 0;

    % Try to correctly colour the arm (if colours are in ply file data)
    for linkIndex = 0:self.model.n
        handles = findobj('Tag', self.model.name);
        h = get(handles,'UserData');
        try 
            h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                                                          , plyData{linkIndex+1}.vertex.green ...
                                                          , plyData{linkIndex+1}.vertex.blue]/255;
            h.link(linkIndex+1).Children.FaceColor = 'interp';
        catch ME_1
            disp(ME_1);
            continue;
        end
    end
end
 
        
    end
end

