function varargout = A2UI3(varargin)
% A2UI3 MATLAB code for A2UI3.fig
%      A2UI3, by itself, creates a new A2UI3 or raises the existing
%      singleton*.
%
%      H = A2UI3 returns the handle to a new A2UI3 or the handle to
%      the existing singleton*.
%
%      A2UI3('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in A2UI3.M with the given input arguments.
%
%      A2UI3('Property','Value',...) creates a new A2UI3 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before A2UI3_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to A2UI3_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help A2UI3

% Last Modified by GUIDE v2.5 14-Oct-2017 04:23:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @A2UI3_OpeningFcn, ...
                   'gui_OutputFcn',  @A2UI3_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before A2UI3 is made visible.
function A2UI3_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to A2UI3 (see VARARGIN)

% Choose default command line output for A2UI3
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

movegui('northwest');

% UIWAIT makes A2UI3 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = A2UI3_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
h=findobj('Type','axes','Tag','guiAxes');                         %Find GUI Axes
axes(h);
axis equal;
%axis square;
axis([-0.6 0.5 -0.5 0.5 -0.81 0.7]);                              % makes the limits for the axis
hold on;
makeScene();                                                      % Calls the Function makeScene which plots all safety CAD
writer = writerArm(get(handles.complexModels, 'Value'), 1);       % Creates an Object of writerArm class which will plot the complex model of arm
block = obstacle('block.ply', [-0.5 -0.3 0.5], eye(3));           % Creates an object of Obstacle which plots the block.ply at a position


% --- Executes on button press in stopButton.
function stopButton_Callback(hObject, eventdata, handles)
% hObject    handle to stopButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in collisionButton.
function collisionButton_Callback(hObject, eventdata, handles)
% hObject    handle to collisionButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function userInput_Callback(hObject, eventdata, handles)
% hObject    handle to userInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of userInput as text
%        str2double(get(hObject,'String')) returns contents of userInput as a double


% --- Executes during object creation, after setting all properties.
function userInput_CreateFcn(hObject, eventdata, handles)
% hObject    handle to userInput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in writeButton.
function writeButton_Callback(hObject, eventdata, handles)
% hObject    handle to writeButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h=findobj('Type','axes','Tag','guiAxes');           
cla(h, 'reset');                                            %Resets the Axis
axis(h);
axis equal;
axis([-0.6 0.5 -0.5 0.5 -0.81 0.7]);                        %Chooses the Limits of the Axis
hold on;
makeScene();                                                % Calls Make scene Function
block = obstacle('block.ply', [-0.5 -0.3 0.5], eye(3));     %Creates an Object of obstacle Class which plots block.ply
writer = writerArm(get(handles.complexModels, 'Value'), 1); % Creates the Object of writerArm Class which creates and plots the arm
inputString = get(handles.userInput, 'string');             %Gets the string from the userInput text box 
plotter = textPlotter(inputString);                         % Create an Object of textPlotter Class with input of text to be written
plotter.graphPlots(plotter);                                % Runs the Graph plots function for the Object plotter of class textPlotter
for i = 1:size(inputString,2)                               % For Loop for the size of inputString
    currChar = String2Letter.conv(inputString, i);          % CurrChar equals the string to letter at each position i 
    X = plotter.plots(i,1);                                 % X position iterating through the plots property of plotter                                                          
    Y = plotter.plots(i,2);                                 % Y position iterating through the plots property of plotter
    charPath = charLines(plotter, currChar, X, Y);          % char Path is the path returned from charLines from textPlotter class with inputs of plotter itself currChar X and Y
    for j = 1:length(charPath)                              % Iterating through the charPath 
        [qMatrix, X2, Y2, ~] = writer.drawChar(writer, charPath, j);    % Creates a qMatrix for the X Y Z positions of the charPath
        for k = 1:length(qMatrix)                           % Iterating through the qMatrix
            check = 1;
            while(check)                                    % While loop
                check = 0;                                  
                blockCheck = (get(handles.slider1, 'Value') - block.position(1,4)); % get the Value of the slider
                if (blockCheck ~= 0)                                                %Move the light curtain block if there is a change
                    block.moveObstacle([blockCheck 0 0]);
                end
                cBlock = collisionCube();                                           % Creates a Object of Collision Cube class
                cubeCheck = cBlock.checkCube(block.position(1:3,4)');               % Checks whether the cube triggers light curtain at the blocks position
                if (cubeCheck > 0)                                                  % if comes back true increment the check
                    check = 1;
                    set(handles.text30, 'visible', 'on');                           % Make light curtain warnings visible
                    set(handles.text31, 'visible', 'on');                           % Make light curtain warnings visible
                    set(handles.lightCurtainText, 'visible', 'on');                 % Make light curtain warnings visible
                else                                                                % If not true
                    set(handles.text30, 'visible', 'off');                          % Make light curtain warnings invisible
                    set(handles.text31, 'visible', 'off');                          % Make light curtain warnings invisible
                    set(handles.lightCurtainText, 'visible', 'off');                % Make light curtain warnings invisible
                end
                drawnow();
            end
            if get(handles.stopButton, 'Value')                                     % If stop button is on 
                emergencyStop(handles, writer.arm.getpos());                        % run the emergency stop function 
            end
            writer.arm.animate(qMatrix(k,:));                                       %animate the writer arm iterating through the qMatrix
            draw(writer, qMatrix(k,:));                                             % Draw from the writer arm iterating through the qMatrix
            drawnow();
        end
        %plot3(X,Y,[0.055 0.055]);
    end
end
homeMatrix = writer.moveAttempt3(writer, writer.home+[0 0.05431 -0.09]);            %Move the robot back to the home position
for m = 1:length(homeMatrix)                                                        % iterates through the movement of the home qMatrix
    writer.arm.animate(homeMatrix(m,:));                                            % Animates the movement back
    pause(0.01);
end

% --- Executes on button press in writeRadio.
function writeRadio_Callback(hObject, eventdata, handles)
% hObject    handle to writeRadio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of writeRadio


% WHILE THE WRITE RADIO BUTTON IS ON
set(handles.uipanel5, 'visible', 'on');                         % Turns the write ui panel to visible
set(handles.uipanel6, 'visible', 'off');                        % turns the controller ui panel invisible
set(handles.teachPanel, 'visible', 'off');                      % turns the teach ui panel invisible
h=findobj('Type','axes','Tag','guiAxes');
cla(h, 'reset');                                                %Clears the Axis
axis(h);
axis equal;
axis([-0.6 0.5 -0.5 0.5 -0.81 0.7]);                            % Limits the Axis
hold on;
makeScene();                                                    %Runs the function makeScene which creates Safety.ply
block = obstacle('block.ply', [-0.5 -0.3 0.5], eye(3));         % Plots the light curtain block
writer = writerArm(get(handles.complexModels, 'Value'), 1);     % Creates the Writer Arm

function updateGUI(handles, q)                                  % Updates the UI values in the text boxes and sliders
set(handles.q1Slider, 'Value', q(1));                           % Value of the q1 slider is q at position 1
set(handles.q1Text, 'String', num2str(q(1)));                   % Value of the q1 textbox is q at position 1
set(handles.q2Slider, 'Value', q(2));                           % Value of the q2 slider is q at position 2
set(handles.q2Text, 'String', num2str(q(2)));                   % Value of the q2 textbox is q at position 2
set(handles.q3Slider, 'Value', q(3));                           % Value of the q3 slider is q at position 3
set(handles.q3Text, 'String', num2str(q(3)));                   % Value of the q3 textbox is q at position 3

% --- Executes on button press in jointControlButton.
function jointControlButton_Callback(hObject, eventdata, handles)
% hObject    handle to jointControlButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%When Joint Control Radio Button is on
set(handles.uipanel5, 'visible', 'off');                    %Turns the writer UI panel invisible                       
set(handles.uipanel6, 'visible', 'on');                     %Turns the Controller UI panel invisible               
set(handles.teachPanel, 'visible', 'on');                   %Turns the Joint Control UI panel invisible
h=findobj('Type','axes','Tag','guiAxes');                   %Finds Axis
cla(h, 'reset');                                            % Clears Axis
hold on;
makeScene();                                                % Creates the Scene Safety.ply
d2r = pi/180;
block = obstacle('block.ply', [-0.5 -0.3 0.5], eye(3));     % Plots the block on the axis
axis equal;
axis([-0.6 0.5 -0.5 0.5 -0.81 0.7]);
writer = writerArm(get(handles.complexModels, 'Value'), 0); % Plots the Writer arm and the complex models
button = get(handles.operationModePanel, 'SelectedObject'); %Button is the selected object in the operation mode panel
radioCheck = get(button, 'String');
while(radioCheck == 'Joint Ctrl')                           % While the Joint control is active
    check = 1;
    while(check)                                            %While Loop 
        check = 0;
        blockCheck = (get(handles.slider1, 'Value') - block.position(1,4)); %Check whether the slider has moved from current position and replot
        if (blockCheck ~= 0)
            block.moveObstacle([blockCheck 0 0]);                           %Move the Block
        end
        cBlock = collisionCube();                                           %Create an Object of collisionCube
        cubeCheck = cBlock.checkCube(block.position(1:3,4)');               %Check the cube against the light curtain using checkCube function
        if (cubeCheck > 0)                                                  %When checkCube is greater than 0
            check = 1;
            set(handles.text30, 'visible', 'on');                           % Turn on Light Curtain stop warnings
            set(handles.text31, 'visible', 'on');                           % Turn on Light Curtain stop warnings
            set(handles.lightCurtainText, 'visible', 'on');                 % Turn on Light Curtain stop warnings
            updateGUI(handles, writer.arm.getpos()/d2r);                    % Update the Values of the GUI
        else                                                                % If check cube is fale
            set(handles.text30, 'visible', 'off');                          % Turn of Light Curtain stop warnings
            set(handles.text31, 'visible', 'off');                          % Turn of Light Curtain stop warnings
            set(handles.lightCurtainText, 'visible', 'off');                % Turn of Light Curtain stop warnings
        end
        drawnow();
    end
    if get(handles.stopButton, 'Value')                                     %If stop button is true
        emergencyStop(handles, writer.arm.getpos());                        %Run the Emergency stop function
    end
    oldQ = writer.arm.getpos();                                             % oldQ is the current position of the robot using serial links getpos() functon
    q = d2r*[get(handles.q1Slider, 'Value'),get(handles.q2Slider, 'Value'),get(handles.q3Slider, 'Value'),0,0]; %New Values of q come from the joint control sliders
    q(4) = -q(2) - q(3);                                                    % q(4) is -q(2)-q(3) therefore returing it back to position
    q(5) = pi/2;                                                            % q(5) is always pi/2 perpendicular to ground
    q = tableCollision(writer, q, oldQ);                                    % check whether new q will have a table collision and if it will collide q = oldQ
    updateGUI(handles, q/d2r);                                              % Update the Values in the GUI
    writer.arm.animate(q);                                                  % Animate the robot through q                                                  
    draw(writer, q);                                                        
    T = writer.arm.fkine(writer.arm.getpos()) * transl(0.05431, 0, -0.125); %forward kinematics of the robot position translated a bit in z and x
    rpy = tr2rpy(T(1:3,1:3));                                               % Roll pitch yaw of homogenous transform matrix
    set(handles.controlX,'String', num2str(round(T(1,4),3)));               % Updates the X value in the text box
    set(handles.controlY,'String', num2str(round(T(2,4),3)));               % Updates the Y value in the text box
    set(handles.controlZ,'String', num2str(round(T(3,4),3)));               % Updates the Z value in the text box
    set(handles.controlRoll,'String',  num2str(round(rpy(1),3)));           % Updates the Roll value in the text box
    set(handles.controlPitch,'String', num2str(round(rpy(2),3)));           % Updates the Pitch value in the text box
    set(handles.controlYaw,'String',   num2str(round(rpy(3),3)));           % Updates the Yaw value in the text box
    button = get(handles.operationModePanel, 'SelectedObject');             % get the selected button on Operations Mode Panel
    radioCheck = get(button, 'String');                                     % button now equals that selected string
    
end




% --- Executes on button press in controllerRadio.
function controllerRadio_Callback(hObject, eventdata, handles)
% hObject    handle to controllerRadio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of controllerRadio
set(handles.uipanel5, 'visible', 'off');                        %Turns Write UI panel invisible
set(handles.teachPanel, 'visible', 'off');                      %Turns Controller UI panel visible
set(handles.uipanel6, 'visible', 'on');                         %Turns Joint UI panel invisible
h=findobj('Type','axes','Tag','guiAxes');                       %finds axis
cla(h, 'reset');                                                %resets axis
hold on;
makeScene();
block = obstacle('block.ply', [-0.5 -0.3 0.5], eye(3));         %Creates block Obstacle ply
axis equal;
axis([-0.6 0.5 -0.5 0.5 -0.81 0.7]);                            %limits axis
id = 1;
joy = vrjoystick(id, 'forcefeedback');                          %runs joystick read force reading

writer = writerArm(get(handles.complexModels, 'Value'), 0);     % Creates the arm model
K = 0.75;
writer.arm.delay = 0.001;
dt = 0.15;                                              % Set time step for simulation (seconds)
q = writer.arm.getpos();                                % q is the writer arms current position
button = get(handles.operationModePanel, 'SelectedObject'); % Gets the selected object in operatiom mode panel
radioCheck = get(button, 'String');                         %Gets string of the button
while(radioCheck == 'Controller')                           % When the radio button string is controller
    check = 1;
    while(check)                                            %While loop while check is 1
        check = 0;
        blockCheck = (get(handles.slider1, 'Value') - block.position(1,4)); %check block position vs slider
        if (blockCheck ~= 0)
            block.moveObstacle([blockCheck 0 0]);                       % If different move block
        end
        cBlock = collisionCube();                                       % Create object of collision cube class
        cubeCheck = cBlock.checkCube(block.position(1:3,4)');           % Check whether cube is in the light curtain
        if (cubeCheck > 0)                                              % If cube check is true
            check = 1;
            set(handles.text30, 'visible', 'on');                       % Turn light curtain warning visible
            set(handles.text31, 'visible', 'on');                       % Turn light curtain warning visible
            set(handles.lightCurtainText, 'visible', 'on');             % Turn light curtain warning visible
        else
            set(handles.text30, 'visible', 'off');                      % Turn light curtain warning invisible
            set(handles.text31, 'visible', 'off');                      % Turn light curtain warning invisible
            set(handles.lightCurtainText, 'visible', 'off');            % Turn light curtain warning invisible
        end
        drawnow();
    end
    if get(handles.stopButton, 'Value')                                 % If stop button is true
        emergencyStop(handles, writer.arm.getpos());                    % Run the emergency stop function
    end
    [axes, buttons, ~] = read(joy);                                     % Read the controller
    x = [K*axes(1); K*-axes(2); K*axes(3); K*axes(5); K*axes(4); K*(buttons(1)-buttons(2))];    % Create the movement bsed on the joysticks or button values times by a constant
    J = writer.arm.jacob0(q);                                                                   %Jacobian at current q
    qdot = inv(J'*J + 0.5*eye(5))*J'*x;                                                         % calculation of q dot, joint velocity to get to next position
    
    for j = 1:5
        if q(j) + qdot(j) < writer.arm.qlim(j,1)                                                % If the value is outside the joint limits then the joint velocity is 0
            qdot(j) = 0; 
        elseif q(j) + qdot(j) > writer.arm.qlim(j,2)                                            % If the value is outside the joint limits then the joint velocity is 0
            qdot(j) = 0; 
        end
    end
    oldQ = q;                                                                                   %Oldq is the current q
    q = q + dt*qdot';                                                                           %New q is the current position plus the change in time multiplied by the q velocity
    q(4) = -q(2) - q(3);                                                                        %Q4 counteracts q 2 and q3
    q(5) = pi/2;                                                                                %Q5 is always perpendicular to ground
    q = tableCollision(writer, q, oldQ);                                                        % Check if the writer will hit the table if so q is oldQ
    writer.arm.animate(q);                                                                      % Animate for q
    draw(writer, q);                                                                            
    T = writer.arm.fkine(writer.arm.getpos()) * transl(0.05431, 0, -0.125);                     % end effector position i
    rpy = tr2rpy(T(1:3,1:3));                                                                   % homogeneous transform matrix to roll pitch yaw
    set(handles.controlX,'String', num2str(round(T(1,4),3)));                                   %Puts the X value for q into the text box
    set(handles.controlY,'String', num2str(round(T(2,4),3)));                                   %Puts the Y value for q into the text box
    set(handles.controlZ,'String', num2str(round(T(3,4),3)));                                   %Puts the Z value for q into the text box
    set(handles.controlRoll,'String',  num2str(round(rpy(1),3)));                               %Puts the Roll value for q into the text box
    set(handles.controlPitch,'String', num2str(round(rpy(2),3)));                               %Puts the Pitch value for q into the text box
    set(handles.controlYaw,'String',   num2str(round(rpy(3),3)));                               %Puts the Yaw value for q into the text box
    button = get(handles.operationModePanel, 'SelectedObject');                                 %The selected object in the operation mode panel might be changed
    radioCheck = get(button, 'String');
end

function newQ = tableCollision(writer, q, oldQ)
T = writer.arm.fkine(q) * transl(0.05431, 0, -0.125);                       %T is the current position for the end effector which the function getpos() plus some X and Z values
if(T(1,4) > -0.2 && T(1,4) < 0.2 && T(2,4) > 0.14 && T(2,4) < 0.32)         % Check that T is within certain values for the table at height 0.055
    if T(3,4) < 0.055                                                       %if the new movement will make it go below the table then revert back to current q
        newQ = oldQ;
    else
        newQ = q;                                                           % Else the newQ is just the selected q
    end
else
    if T(3,4) < 0                                                           %Outside of the writing table if the robot is to go below 0 cancel movement and revert to old q
        newQ = oldQ;
    else
        newQ = q;
    end
end

% --- Executes on button press in addObstacle.
function addObstacle_Callback(hObject, eventdata, handles)
% hObject    handle to addObstacle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of addObstacle


% --- Executes on button press in complexModels.
function complexModels_Callback(hObject, eventdata, handles)
% hObject    handle to complexModels (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of complexModels
h=findobj('Type','axes','Tag','guiAxes');
cla(h, 'reset');
axis equal;
%axis square;
axis([-0.5 0.5 -0.5 0.5 -0.82 0.7]);
hold on;
makeScene();                                                                    %Creates the scene and the safety.ply
writer = writerArm(get(handles.complexModels, 'Value'), 1);                     %Creates the writer arm complex models if the complex models check box is clicked
block = obstacle('block.ply', [-0.5 -0.3 0.5], eye(3));                         % Creates the light curtain collision cube


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over stopButton.
function stopButton_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to stopButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function emergencyStop(handles, q)
set(handles.text30, 'visible', 'on');                               % Turns on the emergency stop warnings
set(handles.text31, 'visible', 'on');                               % Turns on the emergency stop warnings
set(handles.stopButton, 'String', 'Continue');                      % Changes button to continue
set(handles.esbText, 'visible', 'on');                              % Turns on the emergency stop warnings
while(get(handles.stopButton, 'Value'))                             %while stop button is active just update UI values constantly
    updateGUI(handles, q*180/pi);
    pause(0.1);
end
set(handles.text30, 'visible', 'off');                              % When the while loop is broken turn off warning text
set(handles.text31, 'visible', 'off');                              % When the while loop is broken turn off warning text
set(handles.stopButton, 'String', 'Stop!');                         %Change button text back to stop 
set(handles.esbText, 'visible', 'off');                             % When the while loop is broken turn off warning text


set(handles.stopButton, 'String', 'Stop!');

function makeScene()                                                    %Creates scene and safety using the obstacle function for finaldobotsafety.ply
cage = obstacle('FinalDobotSafety.ply', [-0.13 -0.28 0.385], rotz(pi)); %Is positioned at a specific position to centre the part

function draw(writer, q)
planeNormal = [0 0 1];                                                  % Writer line plane intersection
planePnt = [0 0.225 0.055];                                             % Plane points
planeBounds = [-0.2,0.2,0.18,0.315,0.055,0.055];                        % Bounds of the plane
  
pencilStartTr = writer.arm.fkine(q) * transl(0.05431, 0, -0.125);       % Pencil start transform
pencilStartPnt = pencilStartTr(1:3,4)';                                 % Pencil start point from the transform
pencilEndTr = writer.arm.fkine(q) * transl(0.05431,0,-0.145);           % Pencil end transform
pencilEndPnt = pencilEndTr(1:3,4)';                                     %Pencil End point from the transform

[intersectionPoints,check] = LinePlaneIntersection(planeNormal,planePnt,pencilStartPnt,pencilEndPnt);       % Using the line plane intersection script to determine whether the pencil is close enough to write
if check == 1 && planeBounds(1) <= intersectionPoints(1,1) && intersectionPoints(1,1) <= planeBounds(2) && planeBounds(3) <= intersectionPoints(1,2) && intersectionPoints(1,2) <= planeBounds(4)%&& all(planeBounds([1,3,5]) < intersectionPoints) && all(intersectionPoints < planeBounds([2,4,6]))
    try delete(intersectionPointPlot_h);end
    intersectionPointPlot_h = plot3(intersectionPoints(:,1),intersectionPoints(:,2),intersectionPoints(:,3),'b.');           
end
hold on;
drawnow();



% --- Executes on slider movement.
function slider1_Callback(~, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function q1Slider_Callback(hObject, eventdata, handles)
% hObject    handle to q1Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.q1Text, 'String', num2str(get(handles.q1Slider, 'Value')));                     %q1 text box is the value from the q1 slider

% --- Executes during object creation, after setting all properties.
function q1Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q1Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function q2Slider_Callback(hObject, eventdata, handles)
% hObject    handle to q2Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.q2Text, 'String', num2str(get(handles.q2Slider, 'Value')));     %q2 text box is the value from the q2 slider

% --- Executes during object creation, after setting all properties.
function q2Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q2Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function q3Slider_Callback(hObject, eventdata, handles)
% hObject    handle to q3Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.q3Text, 'String', num2str(get(handles.q3Slider, 'Value')));                             %q3 text box is the value from the q3 slider

% --- Executes during object creation, after setting all properties.
function q3Slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to q3Slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



% function XPosInput_Callback(hObject, eventdata, handles)
% % hObject    handle to XPosInput (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hints: get(hObject,'String') returns contents of XPosInput as text
% %        str2double(get(hObject,'String')) returns contents of XPosInput as a double
% 
% 
% % --- Executes during object creation, after setting all properties.
% function XPosInput_CreateFcn(hObject, eventdata, handles)
% % hObject    handle to XPosInput (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    empty - handles not created until after all CreateFcns called
% 
% % Hint: edit controls usually have a white background on Windows.
% %       See ISPC and COMPUTER.
% if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
%     set(hObject,'BackgroundColor','white');
% end
% 
% 
% 
% function YPosInput_Callback(hObject, eventdata, handles)
% % hObject    handle to YPosInput (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hints: get(hObject,'String') returns contents of YPosInput as text
% %        str2double(get(hObject,'String')) returns contents of YPosInput as a double
% 
% 
% % --- Executes during object creation, after setting all properties.
% function YPosInput_CreateFcn(hObject, eventdata, handles)
% % hObject    handle to YPosInput (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    empty - handles not created until after all CreateFcns called
% 
% % Hint: edit controls usually have a white background on Windows.
% %       See ISPC and COMPUTER.
% if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
%     set(hObject,'BackgroundColor','white');
% end
% 
% 
% 
% function ZPosInput_Callback(hObject, eventdata, handles)
% % hObject    handle to ZPosInput (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% % Hints: get(hObject,'String') returns contents of ZPosInput as text
% %        str2double(get(hObject,'String')) returns contents of ZPosInput as a double
% 
% 
% % --- Executes during object creation, after setting all properties.
% function ZPosInput_CreateFcn(hObject, eventdata, handles)
% % hObject    handle to ZPosInput (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    empty - handles not created until after all CreateFcns called
% 
% % Hint: edit controls usually have a white background on Windows.
% %       See ISPC and COMPUTER.
% if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
%     set(hObject,'BackgroundColor','white');
% end



