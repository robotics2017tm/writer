classdef writerArm < handle
    %writerArm Creates arm for writing
    %Creates and moves arm
    
    properties
        arm
        currentPos
        home
        steps
        workspace = [-0.6 0.5 -0.5 0.5 -0.81 0.7];
        models
        qlim
    end
    methods 
        function self = writerArm(models, qlim)                             %Constructor
            if qlim                                                         %Sets joint limits
                self.qlim = pi/180*[-180 180; -180 180; -180 180; -180 180; -180 180];
            else
                self.qlim = pi/180*[-135 135; -80 -5; 15 85; -180 180; -180 180];
            end
            self.getDobot();                                                %Creates an arm
            self.models = models;                                           %Sets the models for the arm
            if models                                                       %Unused option that allowed to switch between complex and simple models
                self.PlotAndColourRobot([0 -70*pi/180 85*pi/180 -15*pi/180 pi/2]);
            else
                self.arm.plot([0 -60*pi/180 85*pi/180 -25*pi/180 pi/2], 'noarrow', 'workspace', self.workspace, 'view', [-70 30]);
            end
            currentPosH = self.arm.fkine(self.arm.getpos());                %Saves current position of the arm
            self.home = currentPosH(1:3,4)';                                %Sets current position as 'home'
            self.currentPos = currentPosH(1:3,4)';                          %Converts home to x,y,z coords
        end
        function getDobot(self)                                             %Creates the Serial Link Arm
            degrad = pi/180;                                                %Conversion factor
            L1 = Link('d', 0.103, 'a', 0, 'alpha', -pi/2, 'qlim' , self.qlim(1,:)); %Arm Links
            L2 = Link('d', 0, 'a', 0.135, 'alpha', 0, 'qlim' , self.qlim(2,:));
            L3 = Link('d', 0, 'a', 0.16012, 'alpha', 0, 'qlim' , self.qlim(3,:));
            L4 = Link('d', 0, 'a', 0, 'alpha', pi/2, 'qlim', self.qlim(4,:));
            L5 = Link('d', 0, 'a', 0, 'alpha', 0, 'offset', degrad*-90, 'qlim', self.qlim(5,:));
            self.arm = SerialLink([L1 L2 L3 L4 L5], 'name', 'Writer');      %Combines links into arm
            self.arm.base = trotz(pi/2);                                    %Rotates arm base
            self.steps = 20;                                                %Sets arm steps
            %cObj.arm.teach();
        end
        function PlotAndColourRobot(self, jointP)                           %Sets models for the arm
            for linkIndex = 0:self.arm.n                                    %Collects model files and associates them with links
                [ faceData, vertexData, plyData{linkIndex+1} ] = plyread(['DobotLink',num2str(linkIndex),'.ply'],'tri'); %#ok<AGROW>
                self.arm.faces{linkIndex+1} = faceData;
                self.arm.points{linkIndex+1} = vertexData;
            end

            % Display robot
            self.arm.plot3d(jointP, 'noarrow', 'workspace', self.workspace, 'view', [-70 30]);  %Plots arm with models
            
            if isempty(findobj(get(gca,'Children'),'Type','Light'))         %Creates a light source if none exist
                camlight
            end
            self.arm.delay = 0;                                             %Sets arm delay

            for linkIndex = 0:self.arm.n                                    %Colours arm models if possible
                handles = findobj('Tag', self.arm.name);
                h = get(handles,'UserData');
                try 
                    h.link(linkIndex+1).Children.FaceVertexCData = [plyData{linkIndex+1}.vertex.red ...
                                                                  , plyData{linkIndex+1}.vertex.green ...
                                                                  , plyData{linkIndex+1}.vertex.blue]/255;
                    h.link(linkIndex+1).Children.FaceColor = 'interp';
                catch ME_1
                    disp(ME_1);
                    continue;
                end
            end
        end
    end
    methods (Static)
        function [qMatrix, X, Y, Z] = drawChar(obj, lines, i)               %Function for taking points and having the arm draw between them
            X = [obj.currentPos(1), lines(i,1)];                            %Extracts X, Y, Z components of points for line drawing
            Y = [obj.currentPos(2), lines(i,2)];
            Z = [obj.currentPos(3), lines(i,3)];

            endPos = lines(i,:);                                            %Sets end position of arm motion
            qMatrix = obj.moveAttempt3(obj, endPos);                        %Generates qMatrix for arm to follow
            
            obj.currentPos = endPos;                                        %Sets end point as new current position
        end
        function qMatrix = moveAttempt3(obj, endPos)                        %Generate path between 2 points using RMRC
            %----
            t = 1;                                                          %Set parameters
            steps = obj.steps;
            deltaT = t/steps;
            epsilon = 0.04;
            W = diag([1 1 1 1 1 1]);
            %----
            m = zeros(steps,1);                                             %Create containers for data
            qMatrix = zeros(steps,5);
            qdot = zeros(steps,5);
            theta = zeros(3,steps);
            x = zeros(3,steps);
            s = lspb(0,1,steps);
                                                                            %Convert to polar coords to simplify accounting for end effector transform
            [thetaEEs, rhoEEs, z] = cart2pol(obj.currentPos(1),obj.currentPos(2),obj.currentPos(3));
            [xStart, yStart, zStart] = pol2cart(thetaEEs, rhoEEs-0.05431, z+0.09);
            [thetaEEe, rhoEEe] = cart2pol(endPos(1), endPos(2));
            [xEnd, yEnd] = pol2cart(thetaEEe, rhoEEe-0.05431);
            
            for i = 1:steps                                                 %Generate path between the two point based on set no. of steps
                x(1,i) = (1-s(i))*xStart + s(i)*xEnd;                       %Creates X & Y Positions between the points
                x(2,i) = (1-s(i))*yStart + s(i)*yEnd;
                x(3,i) = zStart;
                theta(1,i) = 0;
                theta(2,i) = 0;
                theta(3,i) = (1-s(i))*thetaEEs + s(i)*thetaEEe;             %Ensures end effector stays in line with arm
            end
            
            T = [rpy2r(theta(:,1)') x(:,1); zeros(1,3) 1];                  %Creates htf for starting point of each movement
            q0 = obj.arm.getpos();                                          %Sets joint parameters at current position
            qMatrix(1,:) = obj.arm.ikcon(T,q0);                             %Sets first step in the path as current position
            
            for i = 1:steps-1                                               %Loops for correct number of steps
                T = obj.arm.fkine(qMatrix(i,:));                            %Gets end effector position of latest qMatrix entry
                deltaX = x(:,i+1) - T(1:3,4);                               %Calculates change necessary for the next motion
                Rd = rpy2r(theta(:,i+1)');                                  %Deals with rotation of the end effector
                Ra = T(1:3,1:3);
                deltaR = Rd*Ra';
                deltaTheta = tr2rpy(deltaR)';
                xdot = [deltaX;deltaTheta]/deltaT;                          %Calculates movement based on time interval
                xdot = W*xdot;                                              %Adjusts weighting of joints
                J = obj.arm.jacob0(qMatrix(i,:));                           %Calculates jacobian of latest joint parameter entry
                m(i) = sqrt(det(J*J'));                                     %Calculates manipulability
                if m(i) < epsilon                                           %Uses Least squares if necessary to avoid singularities
                    lambda = (1-(m(i)/epsilon)^2)*0.02;
                    invJ = inv(J'*J + lambda*eye(5))*J';
                else
                    invJ = inv(J);
                end
                qdot(i,:) = (invJ*xdot)';
                for j = 1:3                                                 %Checks if next configuration will exceed joint limits
                    if qMatrix(i,j) + deltaT*qdot(i,j) < obj.arm.qlim(j,1) 
                        qdot(i,j) = 0;                                      %Sets change to zero if it will
                    elseif qMatrix(i,j) + deltaT*qdot(i,j) > obj.arm.qlim(j,2) 
                        qdot(i,j) = 0; 
                    end
                end
                qMatrix(i+1,:) = qMatrix(i,:) + deltaT*qdot(i,:);           %Saves latest motion into set of joint parameters
            end
        end
    end
end

