%% Table
 % After saving in blender then load the triangle mesh
[f,v,data] = plyread('DobotSafety.ply','tri');

% Get vertex count
DobotSafetyVertexCount = size(v,1);

% Move center point to origin
midPoint = sum(v)/DobotSafetyVertexCount;
DobotSafetyVerts = v - repmat(midPoint,DobotSafetyVertexCount,1);

vertexColours = [data.vertex.red, data.vertex.green, data.vertex.blue] / 255;

DobotSafetyMesh_h = trisurf(f,v(:,1),v(:,2), v(:,3) ...
    ,'FaceVertexCData',vertexColours,'EdgeColor','interp','EdgeLighting','flat');



% Create a transform to describe the location (at the origin, since it's centered
tablePose = [[eye(3) [0;0;0.2]; zeros(1,3) 1] * [DobotSafetyVerts,ones(DobotSafetyVertexCount,1)]']';

% Scale the colours to be 0-to-1 (they are originally 0-to-255

DobotSafetyMesh_h.Vertices = tablePose(:,1:3);
% Then plot the trisurf